<!DOCTYPE html>
<html lang ="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, intial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h2>Buat Account Baru!</h2>
    <h3>Sign Up Form</h3>
    <form action="/welcome1" method="POST">
        @csrf
        First Name: <br><input type="text" name="fname"><br><br>
        Last Name: <br><input type="text" name="lname"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="gender"/>Male <br>
        <input type="radio" name="gender"/>Female <br>
        <input type="radio" name="gender"/>Other <br><br>
        
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singapore">Singapore</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <label>
            <input type="checkbox" name="bahasa" value="BahasaIndonesia"> Bahasa Indonesia
        </label> <br>
        <label >
            <input type="checkbox" name="bahasa" value="English"> English
        </label><br>
        <label >
            <input type="checkbox" name="bahasa" value="Other"> Other
        </label><br><br>

        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>